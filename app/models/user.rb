class User < ApplicationRecord
  include ActiveModel::SecurePassword
  has_secure_password
  validates :name, uniqueness: true, presence: true
end
