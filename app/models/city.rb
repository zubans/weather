class City < ApplicationRecord
  validates_presence_of :name, message: "Пожалуйста, введите название города"
  validates_presence_of :lat, message: "Пожалуйста, обозначьте расположение города на карте"
end
