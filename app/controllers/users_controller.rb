class UsersController < ApplicationController
  skip_before_action :require_valid_user!
  before_action :reset_session

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.isadmin = true

    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "Вы сосздали аккаунт администратора"
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :password,:password_confirmation, :isadmin)
  end
end