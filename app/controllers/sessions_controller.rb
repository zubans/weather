class SessionsController < ApplicationController
  skip_before_action :require_valid_user!, except: [:destroy]
def create
  reset_session
  @user = User.find_by(name: session_params[:name])
    if @user && @user.authenticate(session_params[:password])
      session[:user_id] = @user.id
      session[:name] = @user.name
      session[:isadmin] = @user.isadmin

      flash[:success] = "Добро пожаловать господин #{@user.name}"
      redirect_to root_path
    else
      flash[:error] = "Error, try again, please"
      redirect_to root_path
    end
end

def destroy
  reset_session
end

def view_user
  redirect_to login_path
end


private

def session_params
  params.require(:session).permit(:name,:password,:isadmin)
end
end