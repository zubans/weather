class CitiesController < ApplicationController
  require 'net/http'
  require 'json'
  def test_connection
    begin
  uri = URI("http://ya.ru")
  req = Net::HTTP::Get.new(uri)
    rescue => e
      @error = e.message
      exception Handling
    end
  end

  def index
    @cities = City.all
  end

  def new

    if session[:isadmin]
      unless @error
    @city = City.new
      end
    else
      redirect_to root_path
    end
    
  end

  def create
    @city = City.new(city_params)
    if @city.save
      flash[:success] = "Object successfully created"
      redirect_to @city
    else
      flash[:error] = "Something went wrong"
      render 'new'
    end
  end

  def show
    
    @city = City.find(params[:id])
    begin
    uri = URI("https://api.weather.yandex.ru/v1/forecast?lat=#{@city.lat}&lon=#{@city.lon}")
    req = Net::HTTP::Get.new(uri)

 
    req['X-Yandex-API-Key'] = "a0c21972-3aea-4613-b10b-16b6afd4ea17"

    res = Net::HTTP.start(uri.host) { |http|
    http.request(req)
    }
    info = res.body
    @weather = JSON.parse(info)
  rescue
    
    @cities = City.all
    flash[:error] = "Отсутствует подключение к серверу погоды" #вывод на индексе
    render 'index'
  end
  end
  
  private

  def city_params
    params.require(:city).permit(:id, :name, :lat, :lon)
  end
  
end
