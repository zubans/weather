class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :slug
      t.string :lat
      t.string :lon
      t.timestamps
    end
  end
end
