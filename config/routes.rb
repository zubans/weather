Rails.application.routes.draw do
 root "cities#index"

 get 'login', to: 'sessions#new'
 get 'logout', to: 'sessions#destroy'

  resources :cities
  resources :sessions, only: [:create]
  resources :users, only: [:new, :create]
end
